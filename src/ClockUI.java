/*
 * This source code is Copyright 2015 by Raksit Mantanacharu
 */
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.Date;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * GUI for showing time and setting alarm.
 * @author Raksit Mantanacharu 5710546402
 * @version 2015.04.05
 */
public class ClockUI extends JFrame {

	/** Labels are used for showing time as digit style (HH : MM : SS). */
	protected JLabel hours1;
	protected JLabel hours2;
	protected JLabel minutes1;
	protected JLabel minutes2;
	protected JLabel seconds1;
	protected JLabel seconds2;
	
	/** Label that show alarm status ON/OFF. */
	private JLabel alarmStatus;
	
	/** DELAY is used for running time timer. */
	private static final int DELAY = 500; //milliseconds

	/** Storing digit images. */
	private ImageIcon[] clockDigits = new ImageIcon[10];
	
	/** Clip is used for opening sound. */
	private Clip clip;
	
	/** Clock (of course). */
	private Clock clock;

	/**
	 * Initializing clock, importing images and audio.
	 * @param clock
	 */
	public ClockUI(Clock clock) {
		
		this.clock = clock;

		importDigits();
		

			try {
				clip = AudioSystem.getClip();
				try {
					clip.open(AudioSystem.getAudioInputStream(new BufferedInputStream(getClass().getResourceAsStream("resource/alarm_sound.wav"))));
				} catch (IOException e) {
					e.printStackTrace();
				} catch (UnsupportedAudioFileException e) {
					e.printStackTrace();
				}
			} catch (LineUnavailableException e) {
				e.printStackTrace();
			}
			
		
		initComponents();
	}

	/**
	 * Initializing components of GUI and starting timer.
	 */
	public void initComponents() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		JPanel alarmBar = new JPanel();
		alarmBar.setBackground(Color.BLACK);
		alarmBar.setLayout(new FlowLayout());
		alarmStatus = new JLabel();
		
		setAlarmStatus();
		
		alarmStatus.setForeground(new Color(214, 255, 0));
		alarmBar.add(alarmStatus);
		panel.add(alarmBar);
		
		JPanel timeBar = new JPanel();
		timeBar.setBackground(Color.BLACK);
		timeBar.setLayout(new FlowLayout());

		hours1 = new JLabel();
		hours2 = new JLabel();
		JLabel colon1 = new JLabel(" : ");
		colon1.setForeground(new Color(214, 255, 0));
		minutes1 = new JLabel();
		minutes2 = new JLabel();
		JLabel colon2 = new JLabel(" : ");
		colon2.setForeground(new Color(214, 255, 0));
		seconds1 = new JLabel();
		seconds2 = new JLabel();

		setDigits(clock.getDate());

		timeBar.add(hours1);
		timeBar.add(hours2);
		timeBar.add(colon1);
		timeBar.add(minutes1);
		timeBar.add(minutes2);
		timeBar.add(colon2);
		timeBar.add(seconds1);
		timeBar.add(seconds2);
		
		ActionListener updateTask = new UpdateTimeListener();
		clock.timer = new Timer(DELAY, updateTask);
		clock.timer.start();

		panel.add(timeBar);
		
		JPanel buttonBar = new JPanel();
		buttonBar.setBackground(Color.BLACK);
		buttonBar.setLayout(new FlowLayout());

		JButton btnSet = new JButton("SET");
		ActionListener setTask = new SetAlarmListener();
		btnSet.setForeground(new Color(214, 255, 0));
		btnSet.setBackground(Color.BLACK);
		btnSet.addActionListener(setTask);

		JButton btnMinus = new JButton("-");
		btnMinus.setForeground(new Color(214, 255, 0));
		btnMinus.setBackground(Color.BLACK);
		ActionListener minusTask = new MinusListener();
		btnMinus.addActionListener(minusTask);

		JButton btnPlus = new JButton("+");
		btnPlus.setForeground(new Color(214, 255, 0));
		btnPlus.setBackground(Color.BLACK);
		MouseListener plusTask = new PlusListener();
		btnPlus.addMouseListener(plusTask);

		buttonBar.add(btnSet);
		buttonBar.add(btnMinus);
		buttonBar.add(btnPlus);
		panel.add(buttonBar);

		this.add(panel);
		this.pack();
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	/** State performs task depending on what kind of state it is. */
	public void performPlus() {
		clock.getClockState().performPlus();
	}

	/** Updating time and GUI. */
	class UpdateTimeListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			clock.updateTime();
			setDigits(clock.getDate());
		}
	}

	class SetAlarmListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			clock.getClockState().performSet();
		}
	}

	/** For toggling between display mode and showing alarm time mode. */
	class PlusListener implements MouseListener {

		public void mouseClicked(MouseEvent arg0) {

		}

		public void mouseEntered(MouseEvent arg0) {

		}

		public void mouseExited(MouseEvent arg0) {

		}

		public void mousePressed(MouseEvent arg0) {
			clock.getClockState().performPlus();
		}

		public void mouseReleased(MouseEvent arg0) {
			if(clock.getClockState() == clock.ShowingAlarmTime)
				clock.getClockState().performPlus();
		}
	}

	class MinusListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			clock.getClockState().performMinus();
		}
	}
	
	/** Alarm alerts continuously unless user presses any button. */
	public void playSound() {
		clip.start();
		clip.loop(Clip.LOOP_CONTINUOUSLY);
	}
	
	/** Stopping sound when user presses button. */
	public void stopSound() {
		clip.stop();
	}
	
	/** Setting alarm status ON/OFF. */
	public void setAlarmStatus() {
		if(clock.hasSet) {
			alarmStatus.setText("ALARM ON");
		}
		else {
			alarmStatus.setText("ALARM OFF");
		}
	}

	/** Setting digits referring to date (can be current date and setting date). */
	public void setDigits(Date date) {
		int hours = date.getHours();
		int minutes = date.getMinutes();
		int seconds = date.getSeconds();

		hours1.setIcon(clockDigits[hours/10]);
		hours2.setIcon(clockDigits[hours%10]);
		minutes1.setIcon(clockDigits[minutes/10]);
		minutes2.setIcon(clockDigits[minutes%10]);
		seconds1.setIcon(clockDigits[seconds/10]);
		seconds2.setIcon(clockDigits[seconds%10]);
	}

	/** Importing digit images. */ 
	public void importDigits() {
		clockDigits[0] = new ImageIcon(getClass().getResource("resource/0.png"));
		clockDigits[1] = new ImageIcon(getClass().getResource("resource/1.png"));
		clockDigits[2] = new ImageIcon(getClass().getResource("resource/2.png"));
		clockDigits[3] = new ImageIcon(getClass().getResource("resource/3.png"));
		clockDigits[4] = new ImageIcon(getClass().getResource("resource/4.png"));
		clockDigits[5] = new ImageIcon(getClass().getResource("resource/5.png"));
		clockDigits[6] = new ImageIcon(getClass().getResource("resource/6.png"));
		clockDigits[7] = new ImageIcon(getClass().getResource("resource/7.png"));
		clockDigits[8] = new ImageIcon(getClass().getResource("resource/8.png"));
		clockDigits[9] = new ImageIcon(getClass().getResource("resource/9.png"));
	}
}