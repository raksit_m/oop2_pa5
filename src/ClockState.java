/*
 * This source code is Copyright 2015 by Raksit Mantanacharu
 */
/**
 * Determining the methods of the clock state (depending on GUI button which user presses).
 * @author Raksit Mantanacharu 5710546402
 * @version 2015.04.05
 */
public interface ClockState {
	
	/** What happens when enter that state. */
	void enterState();
	
	/** What happens when user press the "Set" button. */
	void performSet();
	
	/** What happens when user press the "+" button. */
	void performPlus();
	
	/** What happens when user press the "-" button. */
	void performMinus();
	
	/** What happens when leave state. */
	void leaveState();
	
}
