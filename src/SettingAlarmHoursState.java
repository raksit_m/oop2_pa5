/*
 * This source code is Copyright 2015 by Raksit Mantanacharu
 */
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;

/**
 * Setting alarm time in hours only.
 * @author Raksit Mantanacharu 5710546402
 * @version 2015.04.05
 */
public class SettingAlarmHoursState extends SettingAlarmState {

	private Date date;
	/** This is not a running time timer, it's a blink timer. */
	protected Timer timer;
	/** Checking whether task is blinking. */
	private boolean isBlinking = false;

	/** Interval for timer. */
	private static final int INTERVAL = 500; //milliseconds
	/** Delay for timer. */
	long delay = 1000 - System.currentTimeMillis()%1000;

	/** Initializing clock and date. */
	public SettingAlarmHoursState(Clock clock) {
		super(clock);
		date = clock.getDate();
	}

	/** Task for blinking digits on GUI. */
	class BlinkTask extends TimerTask {
		public void run() {
			if(!isBlinking) {
				clock.ui.hours1.setIcon(new ImageIcon(getClass().getResource("resource/blink.png")));
				clock.ui.hours2.setIcon(new ImageIcon(getClass().getResource("resource/blink.png")));
				isBlinking = true;
			}
			else {
				clock.ui.setDigits(date);
				isBlinking = false;
			}
		}
	}
	
	/**
	 * When user presses "Set" button, change state to setMinutes mode and stop blinking hours
	 * and start blinking minutes.  
	 */
	public void performSet() {
		SettingAlarmMinutesState setMinutes = (SettingAlarmMinutesState) clock.SettingAlarmMinutes;
		clock.setState(setMinutes);
	}
	
	/**
	 * Timer starts doing blinking.
	 */
	public void setBlink() {
		TimerTask blinkTask = new BlinkTask();
		timer = new Timer();
		timer.scheduleAtFixedRate(blinkTask, delay, INTERVAL);
	}

	/**
	 * Increases number and update GUI when user presses "+" button.
	 * If hour is 24 then change to 0. 
	 */
	public void performPlus() {
		int hours = date.getHours() + 1;
		if(hours == 24) hours = 0;
		date.setHours(hours);
		int minutes = date.getMinutes();
		int seconds = date.getSeconds();

		clock.ui.setDigits(date);
	}

	/**
	 * Decreases number and update GUI when user presses "+" button.
	 * If hour is -1 then change to 23. 
	 */
	public void performMinus() {
		int hours = date.getHours() - 1;
		if(hours == -1) hours = 23;
		date.setHours(hours);
		int minutes = date.getMinutes();
		int seconds = date.getSeconds();

		clock.ui.setDigits(date);
	}

	public void enterState() {
		setBlink();
		clock.getTimer().stop();
		clock.hasSet = true;
		clock.ui.setAlarmStatus();
	}

	public void leaveState() {
		timer.cancel();
	}
}
