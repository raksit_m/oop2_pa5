/*
 * This source code is Copyright 2015 by Raksit Mantanacharu
 */
import java.util.Date;

/**
 * Showing alarm time as user has set it (default is 00:00:00).
 * @author Raksit Mantanacharu 5710546402
 * @version 2015.04.05
 */
public class ShowingAlarmTimeState implements ClockState {
	
	private Clock clock;
	private Date alarmTime = new Date();
	
	public ShowingAlarmTimeState(Clock clock) {
		this.clock = clock;
	}
	
	public void setAlarmTime(Date newDate) {
		alarmTime = newDate;
	}

	public void performSet() {
		
	}

	/** Toggle display mode. */
	public void performPlus() {
		DisplayTimeState displayState = (DisplayTimeState) clock.DisplayTime;
		clock.setState(displayState);
	}

	public void performMinus() {
	
	}
	
	public void enterState() {
		clock.getTimer().stop();
	}

	public void leaveState() {
		clock.getTimer().start();
	}
	
}
