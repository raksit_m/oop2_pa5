/*
 * This source code is Copyright 2015 by Raksit Mantanacharu
 */
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;

/**
 * Setting alarm time in minutes only.
 * @author Raksit Mantanacharu 5710546402
 * @version 2015.04.05
 */
public class SettingAlarmMinutesState extends SettingAlarmState {
	
	private Date date;
	/** This is not a running time timer, it's a blink timer. */
	protected Timer timer;
	/** Checking whether task is blinking. */
	private boolean isBlinking = false;

	/** Interval for timer. */
	private static final int INTERVAL = 500; //milliseconds
	/** Delay for timer. */
	long delay = 1000 - System.currentTimeMillis()%1000;
	
	/** Initializing clock and date. */
	public SettingAlarmMinutesState(Clock clock) {
		super(clock);
		date = clock.getDate();
	}
	
	/** Task for blinking digits on GUI. */
	class BlinkTask extends TimerTask {
		public void run() {
			if(!isBlinking) {
				clock.ui.minutes1.setIcon(new ImageIcon(getClass().getResource("resource/blink.png")));
				clock.ui.minutes2.setIcon(new ImageIcon(getClass().getResource("resource/blink.png")));
				isBlinking = true;
			}
			else {
				clock.ui.setDigits(date);
				isBlinking = false;
			}
		}
	}
	
	/**
	 * When user presses "Set" button, change state to setSeconds mode and stop blinking minutes
	 * and start blinking seconds.  
	 */
	public void performSet() {
		SettingAlarmSecondsState setSeconds = (SettingAlarmSecondsState) clock.SettingAlarmSeconds;
		clock.setState(setSeconds);
	}
	
	/**
	 * Timer starts doing blinking.
	 */
	public void setBlink() {
		TimerTask blinkTask = new BlinkTask();
		timer = new Timer();
		timer.scheduleAtFixedRate(blinkTask, delay, INTERVAL);
	}
	
	/**
	 * Increases number and update GUI when user presses "+" button.
	 * If minutes is 60 then change to 0. 
	 */
	public void performPlus() {
		int minutes = date.getMinutes() + 1;
		if(minutes == 60) minutes = 0;
		date.setMinutes(minutes);
		int hours = date.getHours();
		int seconds = date.getSeconds();
		
		clock.ui.setDigits(date);
	}
	
	/**
	 * Decreases number and update GUI when user presses "+" button.
	 * If minutes is -1 then change to 59. 
	 */
	public void performMinus() {
		int minutes = date.getMinutes() - 1;
		if(minutes == -1) minutes = 59;
		date.setMinutes(minutes);
		int hours = date.getHours();
		int seconds = date.getSeconds();
		
		clock.ui.setDigits(date);
	}

	public void enterState() {
		setBlink();
	}

	public void leaveState() {
		timer.cancel();
	}
	
}
