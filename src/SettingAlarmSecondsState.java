/*
 * This source code is Copyright 2015 by Raksit Mantanacharu
 */
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;

/**
 * Setting alarm time in minutes only.
 * @author Raksit Mantanacharu 5710546402
 * @version 2015.04.05
 */
public class SettingAlarmSecondsState extends SettingAlarmState {
	
	private Date date;
	/** Sending alarm time which is set by 3 setting modes to Display mode. */
	private Date newDate = new Date();
	/** This is not a running time timer, it's a blink timer. */
	protected Timer timer;
	/** Checking whether task is blinking. */
	private boolean isBlinking = false;

	/** Interval for timer. */
	private static final int INTERVAL = 500; //milliseconds
	/** Delay for timer. */
	long delay = 1000 - System.currentTimeMillis()%1000;
	
	/** Initializing clock and date. */
	public SettingAlarmSecondsState(Clock clock) {
		super(clock);
		date = clock.getDate();
	}
	
	/** Task for blinking digits on GUI. */
	class BlinkTask extends TimerTask {
		public void run() {
			if(!isBlinking) {
				clock.ui.seconds1.setIcon(new ImageIcon(getClass().getResource("resource/blink.png")));
				clock.ui.seconds2.setIcon(new ImageIcon(getClass().getResource("resource/blink.png")));
				isBlinking = true;
			}
			else {
				clock.ui.setDigits(date);
				isBlinking = false;
			}
		}
	}
	
	/**
	 * Timer starts doing blinking.
	 */
	public void setBlink() {
		TimerTask blinkTask = new BlinkTask();
		timer = new Timer();
		timer.scheduleAtFixedRate(blinkTask, delay, INTERVAL);
	}
	
	/**
	 * When user presses "Set" button, change state to display mode and stop blinking hours
	 * and save the alarm time.
	 */
	public void performSet() {
		DisplayTimeState displayState = (DisplayTimeState) clock.DisplayTime;
		clock.getTimer().start();
		clock.setState(displayState);
		displayState.setAlarmTime(date);
	}
	
	/**
	 * Increases number and update GUI when user presses "+" button.
	 * If seconds is 60 then change to 0. 
	 */
	public void performPlus() {
		int seconds = date.getSeconds() + 1;
		if(seconds == 60) seconds = 0;
		date.setSeconds(seconds);
		int hours = date.getHours();
		int minutes = date.getMinutes();
		
		clock.ui.setDigits(date);
	}
	
	/**
	 * Decreases number and update GUI when user presses "+" button.
	 * If seconds is -1 then change to 59. 
	 */
	public void performMinus() {
		int seconds = date.getSeconds() - 1;
		if(seconds == -1) seconds = 59;
		date.setSeconds(seconds);
		int hours = date.getHours();
		int minutes = date.getMinutes();
		
		clock.ui.setDigits(date);
	}
	
	public Date getAlarmDate() {
		return newDate;
	}

	public void enterState() {
		setBlink();
	}

	public void leaveState() {
		timer.cancel();
	}
	
}
