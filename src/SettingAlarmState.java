/*
 * This source code is Copyright 2015 by Raksit Mantanacharu
 */
/**
 * Abstract class for grouping the resemble setting modes as there are same behaviors in each one.
 * @author Raksit Mantanacharu 5710546402
 * @version 2015.04.05
 */
public abstract class SettingAlarmState implements ClockState {

	protected Clock clock;

	public SettingAlarmState(Clock clock) {
		this.clock = clock;
	}

	public void performSet() {

	}

	public void performPlus() {

	}

	public void performMinus() {

	}
}
