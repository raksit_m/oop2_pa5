/*
 * This source code is Copyright 2015 by Raksit Mantanacharu
 */
import java.util.Date;
import javax.swing.Timer;

/**
 * Clock model as a controller of GUI.
 * @author Raksit Mantanacharu 5710546402
 * @version 2015.04.05
 */
public class Clock {

	/** Clock states. */
	protected ClockState DisplayTime;
	protected ClockState SettingAlarmHours;
	protected ClockState SettingAlarmMinutes;
	protected ClockState SettingAlarmSeconds;
	protected ClockState ShowingAlarmTime;
	protected ClockState RingingAlarm;

	/** Current clock state. */
	private ClockState state;

	/** Timer for running time. */
	protected Timer timer;

	/** Current date. */
	private Date date = new Date();
	
	/** Checking that user has ever set the alarm. */
	protected boolean hasSet = false;
	
	/** Clock GUI. */
	protected ClockUI ui = new ClockUI(this);

	/** Initializing Clock State and set default state as Display time mode. */
	public Clock() {
		DisplayTime = new DisplayTimeState(this);
		SettingAlarmHours = new SettingAlarmHoursState(this);
		SettingAlarmMinutes = new SettingAlarmMinutesState(this);
		SettingAlarmSeconds = new SettingAlarmSecondsState(this);
		ShowingAlarmTime = new ShowingAlarmTimeState(this);
		RingingAlarm = new RingingAlarmState(this);

		this.state = DisplayTime;
		state.enterState();
	}

	/** Changing state. */
	public void setState(ClockState newState) {
		this.state.leaveState();
		newState.enterState();
		this.state = newState;
	}

	/**
	 * @return current date
	 */
	public Date getDate() {
		return date;
	}
	
	/**
	 * @return current Clock state
	 */
	public ClockState getClockState() {
		return state;
	}
	
	/**
	 * @return running time timer
	 */
	public Timer getTimer() {
		return timer;
	}

	/**
	 * Updating time referring to current time and user has ever set alarm.
	 * Notes: if time is as equals as alarm time, then alert sound.
	 */
	public void updateTime() {
		date.setTime(System.currentTimeMillis());
		DisplayTimeState display = (DisplayTimeState) DisplayTime;
		Date alarmTime = display.getAlarmTime();
		if(date.getHours() == alarmTime.getHours() && date.getMinutes() == alarmTime.getMinutes()
				&& date.getSeconds() == alarmTime.getSeconds() && hasSet) {
			setState(RingingAlarm);
			
		}
	}
}
