/*
 * This source code is Copyright 2015 by Raksit Mantanacharu
 */
import java.util.Date;

/**
 * Display current time mode.
 * @author Raksit Mantanacharu 5710546402
 * @version 2015.04.05
 */
public class DisplayTimeState implements ClockState {

	private Clock clock;
	/** Storing alarm time. */
	private Date alarmTime = new Date();

	/** Initializing clock and set default alarm time to 00:00:00 */
	public DisplayTimeState(Clock clock) {
		this.clock = clock;
	}

	/** When user presses "Set" button, change state to SetHoursState and digit starts blinking
	 * and then stop the running time timer.
	 */
	public void performSet() {
		SettingAlarmHoursState setHours = (SettingAlarmHoursState) clock.SettingAlarmHours;
		clock.setState(setHours);
	}

	/**
	 * When user presses "+" button, change state to Showing alarm time mode and update digits
	 * and stop running time timer.
	 */
	public void performPlus() {
		ShowingAlarmTimeState showingState = (ShowingAlarmTimeState) clock.ShowingAlarmTime;
		clock.setState(showingState);
		showingState.setAlarmTime(alarmTime);
		clock.ui.setDigits(alarmTime);
	}

	public void performMinus() {
		clock.hasSet = false;
		clock.ui.setAlarmStatus();
	}

	/**
	 * Storing alarm time from setting mode.
	 * @param newDate as alarm time from setting mode
	 */
	public void setAlarmTime(Date newDate) {
		alarmTime.setHours(newDate.getHours());
		alarmTime.setMinutes(newDate.getMinutes());
		alarmTime.setSeconds(newDate.getSeconds());
	}

	public Date getAlarmTime() {
		return alarmTime;
	}

	public void enterState() {
		if(!clock.hasSet) {
			alarmTime.setHours(0);
			alarmTime.setMinutes(0);
			alarmTime.setSeconds(0);
		}
	}

	@Override
	public void leaveState() {

	}
}
