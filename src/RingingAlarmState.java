/*
 * This source code is Copyright 2015 by Raksit Mantanacharu
 */
/**
 * Ringing alarm mode, alarm alerts endlessly until user presses any button.
 * @author Raksit Mantanacharu 5710546402
 * @version 2015.04.05
 */
public class RingingAlarmState implements ClockState {
	
	private Clock clock;
	
	public RingingAlarmState(Clock clock) {
		this.clock = clock;
	}

	public void performSet() {
		clock.setState(clock.DisplayTime);
	}

	public void performPlus() {
		clock.setState(clock.DisplayTime);
	}

	public void performMinus() {
		clock.setState(clock.DisplayTime);
	}

	public void enterState() {
		clock.ui.playSound();
	}

	public void leaveState() {
		clock.ui.stopSound();
	}
	
}
